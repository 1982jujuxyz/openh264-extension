variables:
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"

  IMAGE_ID: 'e593f441b6ac7536b0e35a4354dc20c6c123f6b5'
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"
  DOCKER_AMD64: "${DOCKER_REGISTRY}/bst14/amd64:${IMAGE_ID}"

  BST: bst --colors

before_script:
- mkdir -p /etc/ssl/CAS
- mkdir -p ~/.config

- |
  if [ -n "$GITLAB_CAS_PUSH_CERT" ] && [ -n "$GITLAB_CAS_PUSH_KEY" ]; then
    echo "$GITLAB_CAS_PUSH_CERT" > /etc/ssl/CAS/server.crt
    echo "$GITLAB_CAS_PUSH_KEY" > /etc/ssl/CAS/server.key

    cat <<EOF >>~/.config/buildstream.conf
  projects:
  EOF
    for project in freedesktop-sdk openh264-extension; do
      cat <<EOF >>~/.config/buildstream.conf
    ${project}:
      artifacts:
  EOF
      if [ -f /cache-certificate/server.crt ]; then
        cat <<EOF >>~/.config/buildstream.conf
      - url: https://local-cas-server:1102
        client-key: /etc/ssl/CAS/server.key
        client-cert: /etc/ssl/CAS/server.crt
        server-cert: /cache-certificate/server.crt
        push: true
  EOF
      fi
      cat <<EOF >>~/.config/buildstream.conf
      - url: https://freedesktop-sdk-cache.codethink.co.uk:11002
        client-key: /etc/ssl/CAS/server.key
        client-cert: /etc/ssl/CAS/server.crt
        push: true
  EOF
    done
  fi

- |
  export RELEASE_CHANNEL=beta
  export BRANCH=2.0beta
  if [ -n "${FLATHUB_REPO_TOKEN}" ] && [ -n "${FLATHUB_REPO_TOKEN_BETA}" ]; then
    if [ -n "${CI_COMMIT_TAG}" ]; then
      export RELEASE_CHANNEL=stable
      export REPO_TOKEN="${FLATHUB_REPO_TOKEN}"
      export BRANCH=2.0
    else
      export REPO_TOKEN="${FLATHUB_REPO_TOKEN_BETA}"
    fi
  fi

stages:
- build
- prepare-publish
- publish
- finish-publish

.build_template: &build
  stage: build
  script:
  - ${BST} -o arch "${ARCH}" -o release_kind "${RELEASE_CHANNEL}" build flatpak-repo.bst

  - |
    set -eu
    if [ "${ARCH}" = "x86_64" ]; then
      ${BST} -o arch "${ARCH}" checkout flatpak-repo.bst --hardlinks repo
      flatpak remote-add --user --no-gpg-verify openh264 repo
      flatpak install --user -v -y openh264 org.freedesktop.Platform.openh264//${BRANCH}
      curl -OL "https://github.com/cisco/openh264/releases/download/v2.0.0/libopenh264-2.0.0-linux64.5.so.bz2"
      bzip2 -d libopenh264-2.0.0-linux64.5.so.bz2
      diff -ruaN libopenh264-2.0.0-linux64.5.so \
        $HOME/.local/share/flatpak/runtime/org.freedesktop.Platform.openh264/x86_64/${BRANCH}/active/files/extra/libopenh264.so.2.0.0
    fi

  dependencies: []
  except:
  - tags
  - master
  - schedules
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs

build_x86_64:
  image: $DOCKER_AMD64
  <<: *build
  tags:
  - x86_64
  - cache_x86_64
  variables:
    ARCH: x86_64

prepare_publish:
  image: $DOCKER_AMD64
  stage: prepare-publish
  tags:
  - x86_64
  script:
  - flat-manager-client create https://hub.flathub.org/ "${RELEASE_CHANNEL}" > publish_build.txt
  artifacts:
    paths:
    - publish_build.txt
  only:
  - master
  - tags
  except:
  - schedules

.publish_template: &publish
  stage: publish
  script:
  - ${BST} -o arch "${ARCH}" build flatpak-repo.bst
  - ${BST} -o arch "${ARCH}" checkout flatpak-repo.bst --hardlinks repo
  - flatpak build-update-repo --generate-static-deltas --prune repo
  - flat-manager-client push $(cat publish_build.txt) repo
  dependencies:
  - prepare_publish
  artifacts:
    when: always
    paths:
    - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
  - tags
  - master
  except:
  - schedules

publish_x86_64:
  image: $DOCKER_AMD64
  <<: *publish
  tags:
  - x86_64
  - cache_x86_64
  variables:
    ARCH: x86_64

finish_publish:
  image: $DOCKER_AMD64
  stage: finish-publish
  tags:
  - x86_64
  script:
  - flat-manager-client commit --wait $(cat publish_build.txt)
  - flat-manager-client publish --wait $(cat publish_build.txt)
  - flat-manager-client purge $(cat publish_build.txt)
  only:
  - tags
  - master
  except:
  - schedules
  dependencies:
  - prepare_publish

finish_publish_failed:
  image: $DOCKER_AMD64
  stage: finish-publish
  tags:
  - x86_64
  script:
  - flat-manager-client purge $(cat publish_build.txt)
  only:
  - tags
  - master
  except:
  - schedules
  when: on_failure
  dependencies:
  - prepare_publish
