#include <stdio.h>
#include <stdlib.h>
#include <bzlib.h>
#include <fcntl.h>
#include <unistd.h>


FILE*
safe_open (char *filename, const char *mode)
{
  FILE *f = fopen (filename, mode);
  if (f == NULL)
  {
    fprintf (stderr, "Failed to open file %s\n", filename);
    exit (1);
  }

  return f;
}


void
extract (BZFILE *source, FILE *target)
{
  const int BUF_SIZE = 64 * 1024;
  int bzerror = BZ_OK;
  size_t bytes_read = BUF_SIZE;
  char buffer[BUF_SIZE];

  do
  {
    bytes_read = BZ2_bzRead (&bzerror, source, buffer, BUF_SIZE);
    if (bzerror == BZ_OK || bzerror == BZ_STREAM_END)
    {
      size_t bytes_written = fwrite (buffer, 1, bytes_read, target);

      if (bytes_written != bytes_read)
      {
        fprintf (stderr,
                 "Short Write: read %zu bytes, but only wrote %zu bytes\n",
                 bytes_read, bytes_written);
        exit (2);
      }
    }
    else
    {
      fprintf (stderr, "Failed to read bzip2 stream: libbzip2 error %d\n",
               bzerror);
      exit (3);
    }
  }
  while (bzerror == BZ_OK);
}


int
main (int argc, char *argv[])
{
  FILE *archive;
  FILE *library;
  int bzerror = BZ_OK; 

  archive = safe_open (OPENH264_BASENAME, "rb");
  library = safe_open ("libopenh264.so." OPENH264_VERSION, "wb");

  BZFILE *bzfile = BZ2_bzReadOpen (&bzerror, archive, 0, 0, NULL, 0);
  if (bzerror != BZ_OK)
  {
    fprintf (stderr, "Failed to open bzip archive %s: libbzip2 error %d\n",
             OPENH264_BASENAME, bzerror);
    BZ2_bzReadClose (&bzerror, bzfile);
    exit (4);
  }

  printf ("Extracting file: %s\n", OPENH264_BASENAME);
  extract (bzfile, library);

  BZ2_bzReadClose(&bzerror, bzfile);
  if (bzerror != BZ_OK)
  {
    fprintf (stderr, "Failed to close: libbzip2 error %d\n", bzerror);
    exit (6);
  }

  fclose (archive);
  fclose (library);

  unlink (OPENH264_BASENAME);

  symlink ("libopenh264.so." OPENH264_VERSION,
           "libopenh264.so." OPENH264_ABI_VERSION);

  symlink ("libopenh264.so."OPENH264_ABI_VERSION, "libopenh264.so");

  return 0;
}
