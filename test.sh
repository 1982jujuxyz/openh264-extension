#!/usr/bin/bash

# Build and checkout flatpak repo
bst build flatpak-repo.bst
bst checkout flatpak-repo.bst repo

# Install the flatpak
flatpak remote-add --user --no-gpg-verify openh264 repo
flatpak install --user -v -y openh264 org.freedesktop.Platform.openh264//2.0

# Compare to binary from cisco
curl -OL "https://github.com/cisco/openh264/releases/download/v2.0.0/libopenh264-2.0.0-linux64.5.so.bz2"
bzip2 -d libopenh264-2.0.0-linux64.5.so.bz2
diff -ruaN libopenh264-2.0.0-linux64.5.so \
$HOME/.local/share/flatpak/runtime/org.freedesktop.Platform.openh264/x86_64/2.0/files/extra/libopenh264.so.2.0.0

# Clean up
flatpak uninstall --user -y org.freedesktop.Platform.openh264//2.0
flatpak remote-delete openh264
rm libopenh264-2.0.0-linux64.5.so
rm -rf repo
